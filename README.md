# ImageLoader

[ ![Download](https://api.bintray.com/packages/wylhyz/maven/imageloader/images/download.svg) ](https://bintray.com/wylhyz/maven/imageloader/_latestVersion)
[![Build Status](https://travis-ci.org/wylhyz/ImageLoader.svg?branch=master)](https://travis-ci.org/wylhyz/ImageLoader)

简易的图片加载框架

使用方法
```java
mImageLoader = ImageLoader.build(MainActivity.this);

mImageLoader.bindBitmap(uri, imageView, mImageWidth, mImageWidth);
```

因为在music player项目里面要解析音乐的封面和专辑封面，所以除了上面的东西之外，还添加了两个专门用于获取封面的参数，具体可以看源码


引入方法

project -> build.gradle
```gradle
allprojects {
    repositories {
        jcenter()

        maven {
            url 'https://dl.bintray.com/wylhyz/maven'
        }
    }
}
```

然后添加下面到项目module的build.gradle下
```gradle
compile 'io.lhyz.android:imageloader:0.3.0'
```